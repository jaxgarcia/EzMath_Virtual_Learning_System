package ui;

import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import domain.User;
import techServ.UserDA;
import techServ.ProgressDA;

public class LoginUI extends JFrame
{
	private JPanel loginPanel;
	private JLabel loginBg, createUserB, existingUserB, createUserL, createUserGoB, existingUserL, existingUserGoB;
	private JTextField nameTF;
	private JComboBox<String> nameCB;
	private Font nameFont, cbNameFont;
	
	private UserDA userDA;
	private ProgressDA progressDA;
	private Connection connection;
	private LoginHandler liHandler;
	
	public LoginUI()
	{
		setTitle("Ez Math Virtual Learning System");
		setSize(1006,728);
		setLayout(null);
		setPanel();
		initializedComponents();
		setComponentsBounds();
		setComponents();
		
		add(loginPanel);
		
		liHandler = new LoginHandler();
		createUserB.addMouseListener(liHandler);
		createUserGoB.addMouseListener(liHandler);
		existingUserB.addMouseListener(liHandler);
		existingUserGoB.addMouseListener(liHandler);
		nameTF.addKeyListener(liHandler);
		
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(false);
		repaint();
	}
	
	private class LoginHandler implements MouseListener, KeyListener
	{
		public void mouseClicked(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) 
		{
			if(createUserB==e.getSource())
				createUserB.setIcon(new ImageIcon(getClass().getResource("createB2.png")));
			else if(existingUserB==e.getSource())
				existingUserB.setIcon(new ImageIcon(getClass().getResource("existingB2.png")));
			else if(createUserGoB==e.getSource())
				createUserGoB.setIcon(new ImageIcon(getClass().getResource("goB2.png")));
			else if(existingUserGoB==e.getSource())
				existingUserGoB.setIcon(new ImageIcon(getClass().getResource("goB2.png")));
		}
		public void mouseExited(MouseEvent e) 
		{
			if(createUserB==e.getSource())
				createUserB.setIcon(new ImageIcon(getClass().getResource("createB1.png")));
			else if(existingUserB==e.getSource())
				existingUserB.setIcon(new ImageIcon(getClass().getResource("existingB1.png")));
			else if(createUserGoB==e.getSource())
				createUserGoB.setIcon(new ImageIcon(getClass().getResource("goB1.png")));
			else if(existingUserGoB==e.getSource())
				existingUserGoB.setIcon(new ImageIcon(getClass().getResource("goB1.png")));
		}
		public void mousePressed(MouseEvent e) 
		{
			if(createUserB==e.getSource())
			{
				loginPanel.remove(loginBg);
				loginPanel.remove(createUserB);
				loginPanel.remove(existingUserB);
				loginPanel.add(nameTF);
				loginPanel.add(createUserL);
				loginPanel.add(createUserGoB);
				loginPanel.add(loginBg);
				repaint();
			}
			else if(existingUserB==e.getSource())
			{
				userDA = new UserDA(getConnection());
				List<User> userList = userDA.getList();
				for(User u : userList)
				{
					nameCB.addItem(u.getUserName());
				}
				loginPanel.remove(loginBg);
				loginPanel.remove(createUserB);
				loginPanel.remove(existingUserB);
				loginPanel.add(nameCB);
				loginPanel.add(existingUserGoB);
				loginPanel.add(existingUserL);
				loginPanel.add(loginBg);
				repaint();
			}
			else if(createUserGoB==e.getSource())
			{
				validateUser();
			}
			else if(existingUserGoB==e.getSource())
			{
				User user = new User();
				String userName = String.valueOf(nameCB.getSelectedItem());
				user.setUserName(userName);
				userDA = new UserDA(getConnection(), user.getUserName());
				List<User> userList = userDA.getList();
				for (User u : userList)
				{
					if(u.getUserName().equals(user.getUserName()))
					{
						user.setUserNo(u.getUserNo());
					}
				}
				new MainUI(user, userDA);
				dispose();
			}
		}
		public void mouseReleased(MouseEvent arg0) {}
		public void keyPressed(KeyEvent e) 
		{
		}
		public void keyReleased(KeyEvent arg0) {
		}
		public void keyTyped(KeyEvent e) 
		{
			if(nameTF.getText().length()>=15)
				e.consume();
		}
		
	}
	
	private void validateUser()
	{	
		User user = new User();
		user.setUserName(nameTF.getText());
		
		userDA = new UserDA(getConnection(), user.getUserName());
		
		
		if(!(nameTF.getText().matches("^[a-zA-Z]+$")))
		{
			JOptionPane.showMessageDialog(null, "Username can't contain spaces, numbers and symbols.\nPlease enter a new username.", 
					"USERNAME ERROR", JOptionPane.INFORMATION_MESSAGE);
		}
		else if(user.getUserName().equals(userDA.getUserToValidate(user).getUserName()))
		{
			JOptionPane.showMessageDialog(null, "Username already exist.\nPlease enter a new username.", 
					"USERNAME ERROR", JOptionPane.INFORMATION_MESSAGE);
		}
		else if(!nameTF.getText().equalsIgnoreCase("") && !(user.getUserName().equals(userDA.getUserToValidate(user).getUserName())))
		{
			userDA.addUser(user);
			userDA = new UserDA(getConnection());
			List<User> userList = userDA.getList();
			for (User u : userList)
			{
				if(u.getUserName().equals(user.getUserName()))
				{
					user.setUserNo(u.getUserNo());
				}
			}
			progressDA = new ProgressDA(getConnection());
			progressDA.initializeProgress(user);
			new MainUI(user, userDA);
			dispose();
		}
	}
	
	public Connection getConnection(){
		try
		{
			Class.forName("com.ibm.db2.jcc.DB2Driver");
			connection = DriverManager.getConnection("jdbc:db2://localhost:50000/ezmath","Jax Garcia", "Jacks9069083");
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return connection;
	}
	
	private void setPanel()
	{
		loginPanel = new JPanel();
		loginPanel.setLayout(null);
		loginPanel.setOpaque(false);
		loginPanel.setBounds(0, 0, 1000, 700);
	}
	
	private void initializedComponents()
	{
		loginBg = new JLabel(new ImageIcon(getClass().getResource("loginBG.png")));
		createUserB = new JLabel(new ImageIcon(getClass().getResource("createB1.png")));
		createUserGoB = new JLabel(new ImageIcon(getClass().getResource("goB1.png")));
		createUserL = new JLabel(new ImageIcon(getClass().getResource("createL.png")));
		existingUserB = new JLabel(new ImageIcon(getClass().getResource("existingB1.png")));
		existingUserGoB = new JLabel(new ImageIcon(getClass().getResource("goB1.png")));
		existingUserL = new JLabel(new ImageIcon(getClass().getResource("existingL.png")));
		nameTF = new JTextField(15);
		nameFont = new Font("GROBOLD", Font.BOLD, 60);
		cbNameFont = new Font("GROBOLD", Font.BOLD, 40);
		nameCB = new JComboBox<String>();
	}
	
	private void setComponentsBounds()
	{
		loginBg.setBounds(0, 0, 1000, 700);
		createUserB.setBounds(320, 300, 360, 100);
		createUserGoB.setBounds(450, 490, 123, 116);
		createUserL.setBounds(260, 260, 482, 199);
		existingUserL.setBounds(260, 260, 508, 203);
		existingUserB.setBounds(320, 450, 360, 100);
		existingUserGoB.setBounds(450, 490, 123, 116);
		nameTF.setBounds(301, 356, 400, 80);
		nameTF.setFont(nameFont);
		nameTF.setHorizontalAlignment(JTextField.CENTER);
		nameCB.setBounds(314, 360, 400, 80);
		nameCB.setFont(cbNameFont);
	}
	
	private void setComponents()
	{
		loginPanel.add(createUserB);
		loginPanel.add(existingUserB);
		loginPanel.add(loginBg);
	}

	public static void main(String[] args)
	{
		new LoginUI();
	}
}
