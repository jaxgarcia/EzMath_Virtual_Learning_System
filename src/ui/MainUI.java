package ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import domain.Progress;
import domain.User;
import techServ.ProgressDA;
import techServ.UserDA;

public class MainUI extends JFrame
{
	private JPanel mainPanel, bannerP, sideP, mainP, countP, addP, subP, chaP;
	private JLabel bannerBg, welcomeL, topicProgL, percentL, chaProgL, sideBg, mainBg, countB, addB, subB, chaB, count1, count2, count3, count4, count5, count6, 
		count7, count8, count9, count10, add1, add2, add3, add4, add5, sub1, sub2, sub3, sub4, sub5, chaM, countChaB, addChaB, 
		subChaB, countCha1, countCha1A, countCha1B, countCha1C, countCha2, countCha2A, countCha2B, countCha3, countCha3A,
		addCha1, addCha1A, addCha1B, addCha1C, addCha2, addCha2A, addCha2B, addCha3, addCha3A, addCha3B, subCha1, subCha1A,
		subCha1B, subCha1C, subCha2, subCha2A, subCha2B, subCha3, subCha3A, subCha3B, chaHomeB, countNextB, countPrevB, addNextB, 
		addPrevB, subNextB, subPrevB, countChaNextB, addChaNextB, subChaNextB;
	private URL countU1, countU2, countU3, countU4, countU5, countU6, countU7, countU8, countU9, countU10;
	private Font userFont;
	private DecimalFormat df;
	private int countTemp, addTemp, subTemp, countChaTemp, countCha2Temp, addChaTemp, subChaTemp, topicUpdate, chaUpdate, chaHomeTemp;
	
	private Progress userProgress;
	private ProgressDA mainProgressDA;
	private User loginUser;
	private Connection connection;
	private MainHandler mHandler;
	
	public MainUI(User user, UserDA userDA)
	{
		setTitle("Ez Math Virtual Learning System");
		setSize(1006,728);
		setLayout(null);
		
		this.loginUser = user;
		mainProgressDA = new ProgressDA(getConnection());
		
		setProgress();
		setPanel();
		initializedComponents();
		setComponentsBounds();
		setComponents();
		
		mainPanel.add(bannerP);
		mainPanel.add(sideP);
		mainPanel.add(mainP);
		add(mainPanel);
		
		mHandler = new MainHandler();
		countB.addMouseListener(mHandler);
		countNextB.addMouseListener(mHandler);
		countPrevB.addMouseListener(mHandler);
		addNextB.addMouseListener(mHandler);
		addPrevB.addMouseListener(mHandler);
		subNextB.addMouseListener(mHandler);
		subPrevB.addMouseListener(mHandler);
		countChaB.addMouseListener(mHandler);
		countCha1A.addMouseListener(mHandler);
		countCha1B.addMouseListener(mHandler);
		countCha1C.addMouseListener(mHandler);
		countCha2A.addMouseListener(mHandler);
		countCha2B.addMouseListener(mHandler);
		countCha3A.addMouseListener(mHandler);
		countChaNextB.addMouseListener(mHandler);
		addChaB.addMouseListener(mHandler);
		addCha1A.addMouseListener(mHandler);
		addCha1B.addMouseListener(mHandler);
		addCha1C.addMouseListener(mHandler);
		addCha2A.addMouseListener(mHandler);
		addCha2B.addMouseListener(mHandler);
		addCha3A.addMouseListener(mHandler);
		addCha3B.addMouseListener(mHandler);
		addChaNextB.addMouseListener(mHandler);
		subChaB.addMouseListener(mHandler);
		subCha1A.addMouseListener(mHandler);
		subCha1B.addMouseListener(mHandler);
		subCha1C.addMouseListener(mHandler);
		subCha2A.addMouseListener(mHandler);
		subCha2B.addMouseListener(mHandler);
		subCha3A.addMouseListener(mHandler);
		subCha3B.addMouseListener(mHandler);
		subChaNextB.addMouseListener(mHandler);
		chaHomeB.addMouseListener(mHandler);
		
		if(userProgress.getTopicProg()>=10)
		{
			addB.setEnabled(true);
			addB.addMouseListener(mHandler);
		}
		if(userProgress.getTopicProg()>=15)
		{
			subB.setEnabled(true);
			subB.addMouseListener(mHandler);
		}
		if(userProgress.getTopicProg()>=20)
		{
			chaB.setEnabled(true);
			chaB.addMouseListener(mHandler);
		}
		
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(false);
		repaint();
	}
	
	private class MainHandler implements MouseListener
	{
		public void mouseClicked(MouseEvent e) 
		{
			
		}
		public void mouseEntered(MouseEvent e)
		{
			if(countNextB==e.getSource())
				countNextB.setIcon(new ImageIcon(getClass().getResource("nextB2.png")));
			else if(countPrevB==e.getSource())
				countPrevB.setIcon(new ImageIcon(getClass().getResource("prevB2.png")));
			else if(addNextB==e.getSource())
				addNextB.setIcon(new ImageIcon(getClass().getResource("nextB2.png")));
			else if(addPrevB==e.getSource())
				addPrevB.setIcon(new ImageIcon(getClass().getResource("prevB2.png")));
			else if(subNextB==e.getSource())
				subNextB.setIcon(new ImageIcon(getClass().getResource("nextB2.png")));
			else if(subPrevB==e.getSource())
				subPrevB.setIcon(new ImageIcon(getClass().getResource("prevB2.png")));
			else if(countChaB==e.getSource())
				countChaB.setIcon(new ImageIcon(getClass().getResource("countB2.png")));
			else if(countCha1A==e.getSource())
				countCha1A.setIcon(new ImageIcon(getClass().getResource("countcha1A2.png")));
			else if(countCha1B==e.getSource())
				countCha1B.setIcon(new ImageIcon(getClass().getResource("countcha1B2.png")));
			else if(countCha1C==e.getSource())
				countCha1C.setIcon(new ImageIcon(getClass().getResource("countcha1C2.png")));
			else if(countCha2A==e.getSource())
				countCha2A.setIcon(new ImageIcon(getClass().getResource("countcha2A2.png")));
			else if(countCha2B==e.getSource())
				countCha2B.setIcon(new ImageIcon(getClass().getResource("countcha2B2.png")));
			else if(countCha3A==e.getSource())
				countCha3A.setIcon(new ImageIcon(getClass().getResource("countcha3A2.png")));
			else if(countChaNextB==e.getSource())
				countChaNextB.setIcon(new ImageIcon(getClass().getResource("nextB2.png")));
			else if(addChaB==e.getSource())
				addChaB.setIcon(new ImageIcon(getClass().getResource("addB2.png")));
			else if(addCha1A==e.getSource())
				addCha1A.setIcon(new ImageIcon(getClass().getResource("addcha1A2.png")));
			else if(addCha1B==e.getSource())
				addCha1B.setIcon(new ImageIcon(getClass().getResource("addcha1B2.png")));
			else if(addCha1C==e.getSource())
				addCha1C.setIcon(new ImageIcon(getClass().getResource("addcha1C2.png")));
			else if(addCha2A==e.getSource())
				addCha2A.setIcon(new ImageIcon(getClass().getResource("addcha2A2.png")));
			else if(addCha2B==e.getSource())
				addCha2B.setIcon(new ImageIcon(getClass().getResource("addcha2B2.png")));
			else if(addCha3A==e.getSource())
				addCha3A.setIcon(new ImageIcon(getClass().getResource("addcha3A2.png")));
			else if(addCha3B==e.getSource())
				addCha3B.setIcon(new ImageIcon(getClass().getResource("addcha3B2.png")));
			else if(addChaNextB==e.getSource())
				addChaNextB.setIcon(new ImageIcon(getClass().getResource("nextB2.png")));
			else if(subChaB==e.getSource())
				subChaB.setIcon(new ImageIcon(getClass().getResource("subB2.png")));
			else if(subCha1A==e.getSource())
				subCha1A.setIcon(new ImageIcon(getClass().getResource("subcha1A2.png")));
			else if(subCha1B==e.getSource())
				subCha1B.setIcon(new ImageIcon(getClass().getResource("subcha1B2.png")));
			else if(subCha1C==e.getSource())
				subCha1C.setIcon(new ImageIcon(getClass().getResource("subcha1C2.png")));
			else if(subCha2A==e.getSource())
				subCha2A.setIcon(new ImageIcon(getClass().getResource("subcha2A2.png")));
			else if(subCha2B==e.getSource())
				subCha2B.setIcon(new ImageIcon(getClass().getResource("subcha2B2.png")));
			else if(subCha3A==e.getSource())
				subCha3A.setIcon(new ImageIcon(getClass().getResource("subcha3A2.png")));
			else if(subCha3B==e.getSource())
				subCha3B.setIcon(new ImageIcon(getClass().getResource("subcha3B2.png")));
			else if(subChaNextB==e.getSource())
				subChaNextB.setIcon(new ImageIcon(getClass().getResource("nextB2.png")));
			else if(chaHomeB==e.getSource())
				chaHomeB.setIcon(new ImageIcon(getClass().getResource("homeB2.png")));
		}
		public void mouseExited(MouseEvent e) 
		{
			if(countNextB==e.getSource())
				countNextB.setIcon(new ImageIcon(getClass().getResource("nextB1.png")));
			else if(countPrevB==e.getSource())
				countPrevB.setIcon(new ImageIcon(getClass().getResource("prevB1.png")));
			else if(addNextB==e.getSource())
				addNextB.setIcon(new ImageIcon(getClass().getResource("nextB1.png")));
			else if(addPrevB==e.getSource())
				addPrevB.setIcon(new ImageIcon(getClass().getResource("prevB1.png")));
			else if(subNextB==e.getSource())
				subNextB.setIcon(new ImageIcon(getClass().getResource("nextB1.png")));
			else if(subPrevB==e.getSource())
				subPrevB.setIcon(new ImageIcon(getClass().getResource("prevB1.png")));
			else if(countChaB==e.getSource())
				countChaB.setIcon(new ImageIcon(getClass().getResource("countB1.png")));
			else if(countCha1A==e.getSource())
				countCha1A.setIcon(new ImageIcon(getClass().getResource("countcha1A1.png")));
			else if(countCha1B==e.getSource())
				countCha1B.setIcon(new ImageIcon(getClass().getResource("countcha1B1.png")));
			else if(countCha1C==e.getSource())
				countCha1C.setIcon(new ImageIcon(getClass().getResource("countcha1C1.png")));
			else if(countChaNextB==e.getSource())
				countChaNextB.setIcon(new ImageIcon(getClass().getResource("nextB1.png")));
			else if(addChaB==e.getSource())
				addChaB.setIcon(new ImageIcon(getClass().getResource("addB1.png")));
			else if(addCha1A==e.getSource())
				addCha1A.setIcon(new ImageIcon(getClass().getResource("addcha1A1.png")));
			else if(addCha1B==e.getSource())
				addCha1B.setIcon(new ImageIcon(getClass().getResource("addcha1B1.png")));
			else if(addCha1C==e.getSource())
				addCha1C.setIcon(new ImageIcon(getClass().getResource("addcha1C1.png")));
			else if(addCha2A==e.getSource())
				addCha2A.setIcon(new ImageIcon(getClass().getResource("addcha2A1.png")));
			else if(addCha2B==e.getSource())
				addCha2B.setIcon(new ImageIcon(getClass().getResource("addcha2B1.png")));
			else if(addCha3A==e.getSource())
				addCha3A.setIcon(new ImageIcon(getClass().getResource("addcha3A1.png")));
			else if(addCha3B==e.getSource())
				addCha3B.setIcon(new ImageIcon(getClass().getResource("addcha3B1.png")));
			else if(addChaNextB==e.getSource())
				addChaNextB.setIcon(new ImageIcon(getClass().getResource("nextB1.png")));
			else if(subChaB==e.getSource())
				subChaB.setIcon(new ImageIcon(getClass().getResource("subB1.png")));
			else if(subCha1A==e.getSource())
				subCha1A.setIcon(new ImageIcon(getClass().getResource("subcha1A1.png")));
			else if(subCha1B==e.getSource())
				subCha1B.setIcon(new ImageIcon(getClass().getResource("subcha1B1.png")));
			else if(subCha1C==e.getSource())
				subCha1C.setIcon(new ImageIcon(getClass().getResource("subcha1C1.png")));
			else if(subCha2A==e.getSource())
				subCha2A.setIcon(new ImageIcon(getClass().getResource("subcha2A1.png")));
			else if(subCha2B==e.getSource())
				subCha2B.setIcon(new ImageIcon(getClass().getResource("subcha2B1.png")));
			else if(subCha3A==e.getSource())
				subCha3A.setIcon(new ImageIcon(getClass().getResource("subcha3A1.png")));
			else if(subCha3B==e.getSource())
				subCha3B.setIcon(new ImageIcon(getClass().getResource("subcha3B1.png")));
			else if(subChaNextB==e.getSource())
				subChaNextB.setIcon(new ImageIcon(getClass().getResource("nextB1.png")));
			else if(chaHomeB==e.getSource())
				chaHomeB.setIcon(new ImageIcon(getClass().getResource("homeB1.png")));
		}
		public void mousePressed(MouseEvent e) 
		{
			if(countB==e.getSource())
			{	
				countTemp=1;
				countB.setIcon(new ImageIcon(getClass().getResource("countB2.png")));
				addB.setIcon(new ImageIcon(getClass().getResource("addB1.png")));
				subB.setIcon(new ImageIcon(getClass().getResource("subB1.png")));
				chaB.setIcon(new ImageIcon(getClass().getResource("challengeB1.png")));
				mainP.remove(mainBg);
				mainP.remove(addP);
				mainP.remove(subP);
				mainP.remove(chaP);
				mainP.add(countP);
				countP.remove(count2);
				countP.remove(count3);
				countP.remove(count4);
				countP.remove(count5);
				countP.remove(count6);
				countP.remove(count7);
				countP.remove(count8);
				countP.remove(count9);
				countP.remove(count10);
				countP.remove(countPrevB);
				countP.add(count1);
				countP.add(countNextB);
				countNextB.setEnabled(true);
				mainP.add(mainBg);
				repaint();
			}
			else if(countNextB==e.getSource())
			{
				countTemp++;
				if(countTemp==2)
				{
					countP.remove(count1);
					countP.add(count2);
					countP.add(countPrevB);
					repaint();
				}
				else if(countTemp==3)
				{
					countP.remove(count2);
					countP.add(count3);
					repaint();
				}
				else if(countTemp==4)
				{
					countP.remove(count3);
					countP.add(count4);
					repaint();
				}
				else if(countTemp==5)
				{
					countP.remove(count4);
					countP.add(count5);
					repaint();
				}
				else if(countTemp==6)
				{
					countP.remove(count5);
					countP.add(count6);
					repaint();
				}
				else if(countTemp==7)
				{
					countP.remove(count6);
					countP.add(count7);
					repaint();
				}
				else if(countTemp==8)
				{
					countP.remove(count7);
					countP.add(count8);
					repaint();
				}
				else if(countTemp==9)
				{
					countP.remove(count8);
					countP.add(count9);
					repaint();
				}
				else if(countTemp==10)
				{
					countP.remove(count9);
					countP.add(count10);
					countNextB.setEnabled(false);
					addB.setEnabled(true);
					addB.addMouseListener(mHandler);
					repaint();
				}
				else if(countTemp==11)
				{
					countTemp--;
					JOptionPane.showMessageDialog(null, "No more topics.\nProcede to addition.", 
							"ERROR", JOptionPane.INFORMATION_MESSAGE);
				}
				
			}
			else if(countPrevB==e.getSource())
			{
				countTemp--;
				if(countTemp==1)
				{
					countP.remove(count2);
					countP.remove(countPrevB);
					countP.add(count1);
					repaint();
				}
				if(countTemp==2)
				{
					countP.remove(count3);
					countP.add(count2);
					repaint();
				}
				else if(countTemp==3)
				{
					countP.remove(count4);
					countP.add(count3);
					repaint();
				}
				else if(countTemp==4)
				{
					countP.remove(count5);
					countP.add(count4);
					repaint();
				}
				else if(countTemp==5)
				{
					countP.remove(count6);
					countP.add(count5);
					repaint();
				}
				else if(countTemp==6)
				{
					countP.remove(count7);
					countP.add(count6);
					repaint();
				}
				else if(countTemp==7)
				{
					countP.remove(count8);
					countP.add(count7);
					repaint();
				}
				else if(countTemp==8)
				{
					countP.remove(count9);
					countP.add(count8);
					repaint();
				}
				else if(countTemp==9)
				{
					countP.remove(count10);
					countNextB.setEnabled(true);
					countP.add(count9);
					repaint();
				}
			}
			else if(addB==e.getSource())
			{
				addTemp = 1;
				countB.setIcon(new ImageIcon(getClass().getResource("countB1.png")));
				addB.setIcon(new ImageIcon(getClass().getResource("addB2.png")));
				subB.setIcon(new ImageIcon(getClass().getResource("subB1.png")));
				chaB.setIcon(new ImageIcon(getClass().getResource("challengeB1.png")));
				mainP.remove(mainBg);
				mainP.remove(countP);
				mainP.remove(subP);
				mainP.remove(chaP);
				mainP.add(addP);
				addP.remove(add2);
				addP.remove(add3);
				addP.remove(add4);
				addP.remove(add5);
				addP.remove(addPrevB);
				addP.add(add1);
				addP.add(addNextB);
				addNextB.setEnabled(true);
				mainP.add(mainBg);
				repaint();
			}
			else if(addNextB==e.getSource())
			{
				addTemp++;
				if(addTemp==2)
				{
					addP.remove(add1);
					addP.add(add2);
					addP.add(addPrevB);
					repaint();
				}
				else if(addTemp==3)
				{
					addP.remove(add2);
					addP.add(add3);
					repaint();
				}
				else if(addTemp==4)
				{
					addP.remove(add3);
					addP.add(add4);
					repaint();
				}
				else if(addTemp==5)
				{
					addP.remove(add4);
					addP.add(add5);
					addNextB.setEnabled(false);
					subB.setEnabled(true);
					subB.addMouseListener(mHandler);
					repaint();
				}
				else if(addTemp==6)
				{
					addTemp--;
					JOptionPane.showMessageDialog(null, "No more topics.\nProcede to subtraction.", 
							"ERROR", JOptionPane.INFORMATION_MESSAGE);
				}
			}
			else if(addPrevB==e.getSource())
			{
				addTemp--;
				if(addTemp==1)
				{
					addP.remove(add2);
					addP.remove(addPrevB);
					addP.add(add1);
					repaint();
				}
				else if(addTemp==2)
				{
					addP.remove(add3);
					addP.add(add2);
					repaint();
				}
				else if(addTemp==3)
				{
					addP.remove(add4);
					addP.add(add3);
					repaint();
				}
				else if(addTemp==4)
				{
					addP.remove(add5);
					addNextB.setEnabled(true);
					addP.add(add4);
					repaint();
				}
			}
			else if(subB==e.getSource())
			{
				subTemp=1;
				countB.setIcon(new ImageIcon(getClass().getResource("countB1.png")));
				addB.setIcon(new ImageIcon(getClass().getResource("addB1.png")));
				subB.setIcon(new ImageIcon(getClass().getResource("subB2.png")));
				chaB.setIcon(new ImageIcon(getClass().getResource("challengeB1.png")));
				mainP.remove(mainBg);
				mainP.remove(countP);
				mainP.remove(addP);
				mainP.remove(chaP);
				mainP.add(subP);
				subP.remove(sub2);
				subP.remove(sub3);
				subP.remove(sub4);
				subP.remove(sub5);
				subP.remove(subPrevB);
				subP.add(sub1);
				subP.add(subNextB);
				subNextB.setEnabled(true);
				mainP.add(mainBg);
				repaint();
			}
			else if(subNextB==e.getSource())
			{
				subTemp++;
				if(subTemp==2)
				{
					subP.remove(sub1);
					subP.add(sub2);
					subP.add(subPrevB);
					repaint();
				}
				else if(subTemp==3)
				{
					subP.remove(sub2);
					subP.add(sub3);
					repaint();
				}
				else if(subTemp==4)
				{
					subP.remove(sub3);
					subP.add(sub4);
					repaint();
				}
				else if(subTemp==5)
				{
					subP.remove(sub4);
					subP.add(sub5);
					subNextB.setEnabled(false);
					chaB.setEnabled(true);
					chaB.addMouseListener(mHandler);
					repaint();
				}
				else if(subTemp==6)
				{
					subTemp--;
					JOptionPane.showMessageDialog(null, "No more topics.\nTry the challenges!", 
							"ERROR", JOptionPane.INFORMATION_MESSAGE);
				}
			}
			else if(subPrevB==e.getSource())
			{
				subTemp--;
				if(subTemp==1)
				{
					subP.remove(sub2);
					subP.remove(subPrevB);
					subP.add(sub1);
					repaint();
				}
				else if(subTemp==2)
				{
					subP.remove(sub3);
					subP.add(sub2);
					repaint();
				}
				else if(subTemp==3)
				{
					subP.remove(sub4);
					subP.add(sub3);
					repaint();
				}
				else if(subTemp==4)
				{
					subP.remove(sub5);
					subP.add(sub4);
					subNextB.setEnabled(true);
					repaint();
				}
			}
			else if(chaB==e.getSource())
			{
				chaB.setIcon(new ImageIcon(getClass().getResource("challengeB2.png")));
				countB.setIcon(new ImageIcon(getClass().getResource("countB1.png")));
				addB.setIcon(new ImageIcon(getClass().getResource("addB1.png")));
				subB.setIcon(new ImageIcon(getClass().getResource("subB1.png")));
				mainP.remove(mainBg);
				mainP.remove(countP);
				mainP.remove(addP);
				mainP.remove(subP);
				mainP.add(chaP);
				chaP.add(chaHomeB);
				chaHomeB.setEnabled(false);
				chaP.add(countChaB);
				chaP.add(addChaB);
				chaP.add(subChaB);
				chaP.add(chaM);
				mainP.add(mainBg);
				repaint();
			}
			else if(countChaB==e.getSource())
			{
				countChaB.setIcon(new ImageIcon(getClass().getResource("countB1.png")));
				countChaTemp=1;
				countCha2Temp=0;
				chaHomeTemp=0;
				sideP.remove(countB);
				sideP.remove(addB);
				sideP.remove(subB);
				sideP.remove(chaB);
				chaP.remove(countChaB);
				chaP.remove(addChaB);
				chaP.remove(subChaB);
				chaP.remove(chaM);
				chaP.add(countCha1A);
				chaP.add(countCha1B);
				chaP.add(countCha1C);
				chaP.add(countCha1);
				repaint();
			}
			else if(countCha1A==e.getSource())
			{
				JOptionPane.showMessageDialog(null, "Please try again.", 
						"WRONG ANSWER", JOptionPane.INFORMATION_MESSAGE);
			}
			else if(countCha1B==e.getSource())
			{
				JOptionPane.showMessageDialog(null, "Please try again.", 
						"WRONG ANSWER", JOptionPane.INFORMATION_MESSAGE);
			}
			else if(countCha1C==e.getSource())
			{
				chaP.add(countChaNextB);
				JOptionPane.showMessageDialog(null, "Good Job!\nClick next for the next question.", 
						"CORRECT ANSWER", JOptionPane.INFORMATION_MESSAGE);
				repaint();
			}
			else if(countCha2A==e.getSource())
			{
				countCha2Temp++;
				countCha2A.setIcon(new ImageIcon(getClass().getResource("countcha2A2.png")));
				if(countCha2Temp>=2)
				{
					chaP.add(countChaNextB);
					JOptionPane.showMessageDialog(null, "Good Job!\nClick next for the next question.", 
							"YOU FOUND THE NUMBERS", JOptionPane.INFORMATION_MESSAGE);
					repaint();	
				}
			}
			else if(countCha2B==e.getSource())
			{
				countCha2Temp++;
				countCha2B.setIcon(new ImageIcon(getClass().getResource("countcha2B2.png")));
				if(countCha2Temp>=2)
				{
					chaP.add(countChaNextB);
					JOptionPane.showMessageDialog(null, "Good Job!\nClick next for the next question.", 
							"YOU FOUND THE NUMBERS", JOptionPane.INFORMATION_MESSAGE);
					repaint();	
				}
			}
			else if(countCha3A==e.getSource())
			{
				chaP.add(chaHomeB);
				JOptionPane.showMessageDialog(null, "You completed the challenges\nfor counting numbers.\nClick Home for more.", 
						"CONGRATULATIONS", JOptionPane.INFORMATION_MESSAGE);
				chaHomeTemp++;
				chaHomeB.setEnabled(true);
				repaint();	
			}
			else if(countChaNextB==e.getSource())
			{
				countChaTemp++;
				if(countChaTemp==2)
				{
					chaP.remove(countCha1A);
					chaP.remove(countCha1B);
					chaP.remove(countCha1C);
					chaP.remove(countCha1);
					chaP.remove(countChaNextB);
					chaP.add(countCha2A);
					chaP.add(countCha2B);
					chaP.add(countCha2);
					repaint();
				}
				else if(countChaTemp==3)
				{
					countCha2A.setIcon(new ImageIcon(getClass().getResource("countcha2A1.png")));
					countCha2B.setIcon(new ImageIcon(getClass().getResource("countcha2B1.png")));
					chaP.remove(countCha2A);
					chaP.remove(countCha2B);
					chaP.remove(countCha2);
					chaP.remove(countChaNextB);
					chaP.add(countCha3A);
					chaP.add(countCha3);
					repaint();
				}
			}
			else if(addChaB==e.getSource())
			{
				addChaB.setIcon(new ImageIcon(getClass().getResource("addB1.png")));
				addChaTemp=1;
				chaHomeTemp=0;
				sideP.remove(countB);
				sideP.remove(addB);
				sideP.remove(subB);
				sideP.remove(chaB);
				chaP.remove(countChaB);
				chaP.remove(addChaB);
				chaP.remove(subChaB);
				chaP.remove(chaM);
				chaHomeB.setEnabled(false);
				chaP.add(addCha1A);
				chaP.add(addCha1B);
				chaP.add(addCha1C);
				chaP.add(addCha1);
				repaint();
			}
			else if(addCha1A==e.getSource())
			{
				chaP.add(addChaNextB);
				JOptionPane.showMessageDialog(null, "Good Job!\nClick next for the next question.", 
						"CORRECT ANSWER", JOptionPane.INFORMATION_MESSAGE);
				repaint();
			}
			else if(addCha1B==e.getSource())
			{
				JOptionPane.showMessageDialog(null, "Please try again.", 
						"WRONG ANSWER", JOptionPane.INFORMATION_MESSAGE);
			}
			else if(addCha1C==e.getSource())
			{
				JOptionPane.showMessageDialog(null, "Please try again.", 
						"WRONG ANSWER", JOptionPane.INFORMATION_MESSAGE);
			}
			else if(addCha2A==e.getSource())
			{
				chaP.add(addChaNextB);
				JOptionPane.showMessageDialog(null, "Good Job!\nClick next for the next question.", 
						"CORRECT ANSWER", JOptionPane.INFORMATION_MESSAGE);
				repaint();
			}
			else if(addCha2B==e.getSource())
			{
				JOptionPane.showMessageDialog(null, "Please try again.", 
						"WRONG ANSWER", JOptionPane.INFORMATION_MESSAGE);
			}
			else if(addCha3A==e.getSource())
			{
				JOptionPane.showMessageDialog(null, "Please try again.", 
						"WRONG ANSWER", JOptionPane.INFORMATION_MESSAGE);
			}
			else if(addCha3B==e.getSource())
			{
				chaHomeTemp++;
				chaHomeB.setEnabled(true);
				JOptionPane.showMessageDialog(null, "You completed the challenges\nfor addition.\nClick Home for more.", 
						"CONGRATULATIONS", JOptionPane.INFORMATION_MESSAGE);
				repaint();	
			}
			else if(addChaNextB==e.getSource())
			{
				addChaTemp++;
				if(addChaTemp==2)
				{
					chaP.remove(addCha1A);
					chaP.remove(addCha1B);
					chaP.remove(addCha1C);
					chaP.remove(addCha1);
					chaP.remove(addChaNextB);
					chaP.add(addCha2A);
					chaP.add(addCha2B);
					chaP.add(addCha2);
					repaint();
				}
				else if(addChaTemp==3)
				{
					chaP.remove(addCha2A);
					chaP.remove(addCha2B);
					chaP.remove(addCha2);
					chaP.remove(addChaNextB);
					chaP.add(addCha3A);
					chaP.add(addCha3B);
					chaP.add(addCha3);
					repaint();
				}
			}
			else if(subChaB==e.getSource())
			{
				subChaB.setIcon(new ImageIcon(getClass().getResource("subB1.png")));
				subChaTemp=1;
				chaHomeTemp=0;
				sideP.remove(countB);
				sideP.remove(addB);
				sideP.remove(subB);
				sideP.remove(chaB);
				chaP.remove(countChaB);
				chaP.remove(addChaB);
				chaP.remove(subChaB);
				chaP.remove(chaM);
				chaHomeB.setEnabled(false);
				chaP.add(subCha1A);
				chaP.add(subCha1B);
				chaP.add(subCha1C);
				chaP.add(subCha1);
				repaint();
			}
			else if(subCha1A==e.getSource())
			{
				JOptionPane.showMessageDialog(null, "Please try again.", 
						"WRONG ANSWER", JOptionPane.INFORMATION_MESSAGE);
			}
			else if(subCha1B==e.getSource())
			{
				chaP.add(subChaNextB);
				JOptionPane.showMessageDialog(null, "Good Job!\nClick next for the next question.", 
						"CORRECT ANSWER", JOptionPane.INFORMATION_MESSAGE);
				repaint();
			}
			else if(subCha1C==e.getSource())
			{
				JOptionPane.showMessageDialog(null, "Please try again.", 
						"WRONG ANSWER", JOptionPane.INFORMATION_MESSAGE);
			}
			else if(subCha2A==e.getSource())
			{
				chaP.add(subChaNextB);
				JOptionPane.showMessageDialog(null, "Good Job!\nClick next for the next question.", 
						"CORRECT ANSWER", JOptionPane.INFORMATION_MESSAGE);
				repaint();
			}
			else if(subCha2B==e.getSource())
			{
				JOptionPane.showMessageDialog(null, "Please try again.", 
						"WRONG ANSWER", JOptionPane.INFORMATION_MESSAGE);
			}
			else if(subCha3A==e.getSource())
			{
				JOptionPane.showMessageDialog(null, "Please try again.", 
						"WRONG ANSWER", JOptionPane.INFORMATION_MESSAGE);
			}
			else if(subCha3B==e.getSource())
			{
				chaHomeTemp++;
				chaHomeB.setEnabled(true);
				JOptionPane.showMessageDialog(null, "You completed the challenges\nfor subtraction.\nClick Home for more.", 
						"CONGRATULATIONS", JOptionPane.INFORMATION_MESSAGE);
				repaint();	
			}
			else if(subChaNextB==e.getSource())
			{
				subChaTemp++;
				if(subChaTemp==2)
				{
					chaP.remove(subCha1A);
					chaP.remove(subCha1B);
					chaP.remove(subCha1C);
					chaP.remove(subCha1);
					chaP.remove(subChaNextB);
					chaP.add(subCha2A);
					chaP.add(subCha2B);
					chaP.add(subCha2);
					repaint();
				}
				else if(subChaTemp==3)
				{
					chaP.remove(subCha2A);
					chaP.remove(subCha2B);
					chaP.remove(subCha2);
					chaP.remove(subChaNextB);
					chaP.add(subCha3A);
					chaP.add(subCha3B);
					chaP.add(subCha3);
					repaint();
				}
			}
			else if(chaHomeB==e.getSource())
			{
				if(chaHomeTemp==1)
				{
					mouseReleased(e);
					chaHomeTemp--;
					countCha3A.setIcon(new ImageIcon(getClass().getResource("countcha3A1.png")));
					sideP.remove(sideBg);
					sideP.add(countB);
					sideP.add(addB);
					sideP.add(subB);
					sideP.add(chaB);
					sideP.add(sideBg);
					chaP.remove(countCha3A);
					chaP.remove(countCha3);
					chaP.remove(addCha3A);
					chaP.remove(addCha3B);
					chaP.remove(addCha3);
					chaP.remove(subCha3A);
					chaP.remove(subCha3B);
					chaP.remove(subCha3);
					chaHomeB.setEnabled(false);
					chaP.add(countChaB);
					chaP.add(addChaB);
					chaP.add(subChaB);
					chaP.add(chaM);
					repaint();
				}
				else
					JOptionPane.showMessageDialog(null, "Finish the challenge first!", 
							"ERROR", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		public void mouseReleased(MouseEvent e)
		{
			if(countB==e.getSource())
			{
				if(userProgress.getTopicProg()==0 && countTemp==1)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
			}
			else if(countNextB==e.getSource())
			{
				if(userProgress.getTopicProg()==1  && countTemp==2)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==2 && countTemp==3)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==3 && countTemp==4)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==4 && countTemp==5)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==5 && countTemp==6)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==6 && countTemp==7)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==7 && countTemp==8)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==8 && countTemp==9)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==9 && countTemp==10)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
			}
			else if(addB==e.getSource())
			{
				if(userProgress.getTopicProg()==10 && addTemp==1)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
			}
			else if(addNextB==e.getSource())
			{
			
				if(userProgress.getTopicProg()==11 && addTemp==2)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==12 && addTemp==3)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==13 && addTemp==4)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==14 && addTemp==5)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
			}
			else if(subB==e.getSource())
			{
				if(userProgress.getTopicProg()==15 && subTemp==1)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
			}
			else if(subNextB==e.getSource())
			{
				if(userProgress.getTopicProg()==16 && subTemp==2)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==17 && subTemp==3)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==18 && subTemp==4)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getTopicProg()==19 && subTemp==5)
				{
					topicUpdate = userProgress.getTopicProg() + 1;
					mainProgressDA.updateTopicProg(loginUser, topicUpdate);
					topicProgL.setText(String.valueOf(topicUpdate + "/20"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
			}
			else if(chaHomeB==e.getSource())
			{
				if(userProgress.getChaProg()==0 && chaHomeTemp==1)
				{
					chaUpdate = userProgress.getChaProg() + 1;
					mainProgressDA.updateChaProg(loginUser, chaUpdate);
					chaProgL.setText(String.valueOf(chaUpdate + "/3"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getChaProg()==1 && chaHomeTemp==1 )
				{
					chaUpdate = userProgress.getChaProg() + 1;
					mainProgressDA.updateChaProg(loginUser, chaUpdate);
					chaProgL.setText(String.valueOf(chaUpdate + "/3"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
				else if(userProgress.getChaProg()==2 && chaHomeTemp==1)
				{
					chaUpdate = userProgress.getChaProg() + 1;
					mainProgressDA.updateChaProg(loginUser, chaUpdate);
					chaProgL.setText(String.valueOf(chaUpdate + "/3"));
					setProgress();
					percentL.setText(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
					repaint();
				}
			}
		}
	}
	
	public void setProgress()
	{
		userProgress = new Progress();
		mainProgressDA = new ProgressDA(getConnection());
		List<Progress> progList = mainProgressDA.getList();
		
		for(Progress p : progList)
		{
			if(p.getUserNo().equals(loginUser.getUserNo()))
			{
				userProgress.setUserNo(p.getUserNo());
				userProgress.setTopicProg(p.getTopicProg());
				userProgress.setChaProg(p.getChaProg());
			}
		}
	}
	
	public double computePercent(int topicValue, int chaValue)
	{
		double a= topicValue, b=chaValue;
		double result = ((a + b)/23) * 100;
		return result;
	}
	
	public Connection getConnection(){
		try
		{
			Class.forName("com.ibm.db2.jcc.DB2Driver");
			connection = DriverManager.getConnection("jdbc:db2://localhost:50000/ezmath","Jax Garcia", "Jacks9069083");
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return connection;
	}

	public void setPanel()
	{
		mainPanel = new JPanel();
		mainPanel.setLayout(null);
		mainPanel.setOpaque(false);
		mainPanel.setBounds(0, 0, 1000, 700);
		
		bannerP = new JPanel();
		bannerP.setLayout(null);
		bannerP.setOpaque(false);
		bannerP.setBounds(0, 0, 1000, 160);
		
		sideP = new JPanel();
		sideP.setLayout(null);
		sideP.setOpaque(false);
		sideP.setBounds(0, 160, 250, 540);
		
		mainP = new JPanel();
		mainP.setLayout(null);
		mainP.setOpaque(false);
		mainP.setBounds(250, 160, 750, 540);
		
		countP = new JPanel();
		countP.setLayout(null);
		countP.setOpaque(false);
		countP.setBounds(0, 0, 750, 540);
		
		addP = new JPanel();
		addP.setLayout(null);
		addP.setOpaque(false);
		addP.setBounds(0, 0, 750, 540);
		
		subP = new JPanel();
		subP.setLayout(null);
		subP.setOpaque(false);
		subP.setBounds(0, 0, 750, 540);
		
		chaP = new JPanel();
		chaP.setLayout(null);
		chaP.setOpaque(false);
		chaP.setBounds(0, 0, 750, 540);
	}
	
	public void initializedComponents()
	{
		//banner
		userFont = new Font("GROBOLD", Font.BOLD, 30);
		df = new DecimalFormat("###.##");
		bannerBg = new JLabel(new ImageIcon(getClass().getResource("BannerBG.png")));
		welcomeL = new JLabel(loginUser.getUserName());
		welcomeL.setFont(userFont);
		welcomeL.setForeground(Color.GREEN);
		topicProgL = new JLabel(String.valueOf(userProgress.getTopicProg()) + "/20");
		topicProgL.setFont(userFont);
		topicProgL.setForeground(Color.GREEN);
		chaProgL = new JLabel(String.valueOf(userProgress.getChaProg()) + "/3");
		chaProgL.setFont(userFont);
		chaProgL.setForeground(Color.GREEN);
		percentL = new JLabel(String.valueOf(df.format(computePercent(userProgress.getTopicProg(),userProgress.getChaProg()))) + "%");
		percentL.setFont(userFont);
		percentL.setForeground(Color.GREEN);
		
		//side panel
		sideBg = new JLabel(new ImageIcon(getClass().getResource("sidePanelBG.png")));
		countB = new JLabel(new ImageIcon(getClass().getResource("countB1.png")));
		addB = new JLabel(new ImageIcon(getClass().getResource("addB1.png")));
		subB = new JLabel(new ImageIcon(getClass().getResource("subB1.png")));
		chaB = new JLabel(new ImageIcon(getClass().getResource("challengeB1.png")));
		
		//main panel
		mainBg = new JLabel(new ImageIcon(getClass().getResource("mainPanelBG.png")));
		
		//count panel
		countU1 = this.getClass().getResource("counting1.gif");
		count1 = new JLabel(new ImageIcon(countU1));
		countU2 = this.getClass().getResource("counting2.gif");
		count2 = new JLabel(new ImageIcon(countU2));
		countU3 = this.getClass().getResource("counting3.gif");
		count3 = new JLabel(new ImageIcon(countU3));
		countU4 = this.getClass().getResource("counting4.gif");
		count4 = new JLabel(new ImageIcon(countU4));
		countU5 = this.getClass().getResource("counting5.gif");
		count5 = new JLabel(new ImageIcon(countU5));
		countU6 = this.getClass().getResource("counting6.gif");
		count6 = new JLabel(new ImageIcon(countU6));
		countU7 = this.getClass().getResource("counting7.gif");
		count7 = new JLabel(new ImageIcon(countU7));
		countU8 = this.getClass().getResource("counting8.gif");
		count8 = new JLabel(new ImageIcon(countU8));
		countU9 = this.getClass().getResource("counting9.gif");
		count9 = new JLabel(new ImageIcon(countU9));
		countU10 = this.getClass().getResource("counting10.gif");
		count10 = new JLabel(new ImageIcon(countU10));
		countNextB = new JLabel(new ImageIcon(getClass().getResource("nextB1.png")));
		countPrevB = new JLabel(new ImageIcon(getClass().getResource("prevB1.png")));
		
		//add panel
		add1 = new JLabel(new ImageIcon(getClass().getResource("addition1.png")));
		add2 = new JLabel(new ImageIcon(getClass().getResource("addition2.png")));
		add3 = new JLabel(new ImageIcon(getClass().getResource("addition3.png")));
		add4 = new JLabel(new ImageIcon(getClass().getResource("addition4.png")));
		add5 = new JLabel(new ImageIcon(getClass().getResource("addition5.png")));
		addNextB = new JLabel(new ImageIcon(getClass().getResource("nextB1.png")));
		addPrevB = new JLabel(new ImageIcon(getClass().getResource("prevB1.png")));
		
		//sub panel
		sub1 = new JLabel(new ImageIcon(getClass().getResource("subtraction1.png")));
		sub2 = new JLabel(new ImageIcon(getClass().getResource("subtraction2.png")));
		sub3 = new JLabel(new ImageIcon(getClass().getResource("subtraction3.png")));
		sub4 = new JLabel(new ImageIcon(getClass().getResource("subtraction4.png")));
		sub5 = new JLabel(new ImageIcon(getClass().getResource("subtraction5.png")));
		subNextB = new JLabel(new ImageIcon(getClass().getResource("nextB1.png")));
		subPrevB = new JLabel(new ImageIcon(getClass().getResource("prevB1.png")));
		
		//cha panel
		chaM = new JLabel(new ImageIcon(getClass().getResource("challengeM.png")));
		chaHomeB = new JLabel(new ImageIcon(getClass().getResource("homeB1.png")));
		
		//count challenge
		countChaB = new JLabel(new ImageIcon(getClass().getResource("countB1.png")));
		countCha1 = new JLabel(new ImageIcon(getClass().getResource("countcha1.png")));
		countCha1A = new JLabel(new ImageIcon(getClass().getResource("countcha1A1.png")));
		countCha1B = new JLabel(new ImageIcon(getClass().getResource("countcha1B1.png")));
		countCha1C = new JLabel(new ImageIcon(getClass().getResource("countcha1C1.png")));
		countCha2 = new JLabel(new ImageIcon(getClass().getResource("countcha2.png")));
		countCha2A = new JLabel(new ImageIcon(getClass().getResource("countcha2A1.png")));
		countCha2B = new JLabel(new ImageIcon(getClass().getResource("countcha2B1.png")));
		countCha3 = new JLabel(new ImageIcon(getClass().getResource("countcha3.png")));
		countCha3A = new JLabel(new ImageIcon(getClass().getResource("countcha3A1.png")));
		countChaNextB = new JLabel(new ImageIcon(getClass().getResource("nextB1.png")));
		
		//add challenge
		addChaB = new JLabel(new ImageIcon(getClass().getResource("addB1.png")));
		addCha1 = new JLabel(new ImageIcon(getClass().getResource("addcha1.png")));
		addCha1A = new JLabel(new ImageIcon(getClass().getResource("addcha1A1.png")));
		addCha1B = new JLabel(new ImageIcon(getClass().getResource("addcha1B1.png")));
		addCha1C = new JLabel(new ImageIcon(getClass().getResource("addcha1C1.png")));
		addCha2 = new JLabel(new ImageIcon(getClass().getResource("addcha2.png")));
		addCha2A = new JLabel(new ImageIcon(getClass().getResource("addcha2A1.png")));
		addCha2B = new JLabel(new ImageIcon(getClass().getResource("addcha2B1.png")));
		addCha3 = new JLabel(new ImageIcon(getClass().getResource("addcha3.png")));
		addCha3A = new JLabel(new ImageIcon(getClass().getResource("addcha3A1.png")));
		addCha3B = new JLabel(new ImageIcon(getClass().getResource("addcha3B1.png")));
		addChaNextB = new JLabel(new ImageIcon(getClass().getResource("nextB1.png")));
		
		//sub challenge
		subChaB = new JLabel(new ImageIcon(getClass().getResource("subB1.png")));
		subCha1 = new JLabel(new ImageIcon(getClass().getResource("subcha1.png")));
		subCha1A = new JLabel(new ImageIcon(getClass().getResource("subcha1A1.png")));
		subCha1B = new JLabel(new ImageIcon(getClass().getResource("subcha1B1.png")));
		subCha1C = new JLabel(new ImageIcon(getClass().getResource("subcha1C1.png")));
		subCha2 = new JLabel(new ImageIcon(getClass().getResource("subcha2.png")));
		subCha2A = new JLabel(new ImageIcon(getClass().getResource("subcha2A1.png")));
		subCha2B = new JLabel(new ImageIcon(getClass().getResource("subcha2B1.png")));
		subCha3 = new JLabel(new ImageIcon(getClass().getResource("subcha3.png")));
		subCha3A = new JLabel(new ImageIcon(getClass().getResource("subcha3A1.png")));
		subCha3B = new JLabel(new ImageIcon(getClass().getResource("subcha3B1.png")));
		subChaNextB = new JLabel(new ImageIcon(getClass().getResource("nextB1.png")));
		
	}
	
	public void setComponentsBounds()
	{
		//banner
		bannerBg.setBounds(0, 0, 1000, 160);
		welcomeL.setBounds(620, 13, 350, 30);
		topicProgL.setBounds(815, 40, 130, 35);
		chaProgL.setBounds(815, 70, 130, 35);
		percentL.setBounds(815, 100, 170, 35);
		
		//side panel
		sideBg.setBounds(0, 0, 250, 540);
		countB.setBounds(20, 80, 210, 90);
		addB.setBounds(20, 190, 210, 90);
		subB.setBounds(20, 300, 210, 90);
		chaB.setBounds(20, 410, 210, 90);
		
		//main panel
		mainBg.setBounds(0, 0, 750, 540);
		
		//count panel
		count1.setBounds(25, 26, 700, 400);
		count2.setBounds(25, 26, 700, 400);
		count3.setBounds(25, 26, 700, 400);
		count4.setBounds(25, 26, 700, 400);
		count5.setBounds(25, 26, 700, 400);
		count6.setBounds(25, 26, 700, 400);
		count7.setBounds(25, 26, 700, 400);
		count8.setBounds(25, 26, 700, 400);
		count9.setBounds(25, 26, 700, 400);
		count10.setBounds(25, 26, 700, 400);
		countNextB.setBounds(620, 440, 90, 90);
		countPrevB.setBounds(500, 440, 90, 90);
		
		//add panel
		add1.setBounds(25, 26, 700, 400);
		add2.setBounds(25, 26, 700, 400);
		add3.setBounds(25, 26, 700, 400);
		add4.setBounds(25, 26, 700, 400);
		add5.setBounds(25, 26, 700, 400);
		addNextB.setBounds(620, 440, 90, 90);
		addPrevB.setBounds(500, 440, 90, 90);
		
		//sub panel
		sub1.setBounds(25, 26, 700, 400);
		sub2.setBounds(25, 26, 700, 400);
		sub3.setBounds(25, 26, 700, 400);
		sub4.setBounds(25, 26, 700, 400);
		sub5.setBounds(25, 26, 700, 400);
		subNextB.setBounds(620, 440, 90, 90);
		subPrevB.setBounds(500, 440, 90, 90);
		
		//cha panel
		chaM.setBounds(25, 26, 700, 400);
		chaHomeB.setBounds(330, 440, 90, 90);
		
		//count challenge
		countChaB.setBounds(120, 150, 210, 90);
		countCha1.setBounds(25, 26, 700, 400);
		countCha1A.setBounds(450, 100, 180, 90);
		countCha1B.setBounds(450, 210, 180, 90);
		countCha1C.setBounds(450, 320, 180, 90);
		countCha2.setBounds(25, 26, 700, 400);
		countCha2A.setBounds(500, 255, 31, 39);
		countCha2B.setBounds(220, 245, 19, 23);
		countCha3.setBounds(25, 26, 700, 400);
		countCha3A.setBounds(338, 333, 47, 61);
		countChaNextB.setBounds(620, 440, 90, 90);
		
		//add challenge
		addChaB.setBounds(420, 150, 210, 90);
		addCha1.setBounds(25, 26, 700, 400);
		addCha1A.setBounds(80, 250, 180, 90);
		addCha1B.setBounds(290, 250, 180, 90);
		addCha1C.setBounds(490, 250, 180, 90);
		addCha2.setBounds(25, 26, 700, 400);
		addCha2A.setBounds(375, 180, 323, 80);
		addCha2B.setBounds(410, 320, 260, 80);
		addCha3.setBounds(25, 26, 700, 400);
		addCha3A.setBounds(190, 175, 281, 81);
		addCha3B.setBounds(220, 295, 210, 122);
		addChaNextB.setBounds(620, 440, 90, 90);
		
		//sub challenge
		subChaB.setBounds(270, 270, 210, 90);
		subCha1.setBounds(25, 26, 700, 400);
		subCha1A.setBounds(80, 250, 180, 90);
		subCha1B.setBounds(290, 250, 180, 90);
		subCha1C.setBounds(490, 250, 180, 90);
		subCha2.setBounds(25, 26, 700, 400);
		subCha2A.setBounds(450, 170, 239, 83);
		subCha2B.setBounds(480, 300, 177, 83);
		subCha3.setBounds(25, 26, 700, 400);
		subCha3A.setBounds(480, 170, 183, 73);
		subCha3B.setBounds(515, 310, 122, 73);
		subChaNextB.setBounds(620, 440, 90, 90);
	}
	
	public void setComponents()
	{
		//banner
		bannerP.add(welcomeL);
		bannerP.add(topicProgL);
		bannerP.add(chaProgL);
		bannerP.add(percentL);
		bannerP.add(bannerBg);
		
		//side panel
		sideP.add(countB);
		sideP.add(addB);
		addB.setEnabled(false);
		sideP.add(subB);
		subB.setEnabled(false);
		sideP.add(chaB);
		chaB.setEnabled(false);
		sideP.add(sideBg);
		
		//main panel
		mainP.add(mainBg);
	}
}