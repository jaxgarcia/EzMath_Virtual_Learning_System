package domain;

public class Progress 
{
	private String userNo;
	private int topicProg;
	private int chaProg;
	
	public String getUserNo(){
		return userNo;
	}
	
	public int getTopicProg(){
		return topicProg;
	}
	
	public int getChaProg(){
		return chaProg;
	}
	
	public String setUserNo(String userNo){
		return this.userNo = userNo;
	}
	
	public int setTopicProg(int topicProg){
		return this.topicProg = topicProg;
	}
	
	public int setChaProg(int chaProg){
		return this.chaProg = chaProg;
	}

}
