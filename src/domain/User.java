package domain;

public class User
{
	private String userNo;
	private String userName;
	
	public String getUserNo(){
		return userNo;
	}
	
	public String getUserName(){
		return userName;
	}
	
	public String setUserNo(String userNo){
		return this.userNo = userNo;
	}
	
	public String setUserName(String userName){
		return this.userName = userName;
	}
	

}
