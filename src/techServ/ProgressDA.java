package techServ;

import java.sql.*;
import java.util.*;

import domain.Progress;
import domain.User;

public class ProgressDA 
{
	private List <Progress> progList;
	private ResultSet rs;
	private Progress prog;
	private Connection connection;
	private String loginUser="";
	
	public ProgressDA (Connection connection)
	{
		this.loginUser = loginUser;
		this.connection = connection;
		initializeList();
	}
	
	public void initializeList()
	{
		try
		{
			progList = new ArrayList<Progress>();
			PreparedStatement s = null;
			String query;
			query = "SELECT * FROM progress ORDER BY 1";
					
			s = connection.prepareStatement(query);
			rs = s.executeQuery();
					
			while (rs.next())
			{			
				prog = new Progress();
				
				prog.setUserNo(rs.getString(1));
				prog.setTopicProg(rs.getInt(2));
				prog.setChaProg(rs.getInt(3));
				
				progList.add(prog);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public List<Progress> getList()
	{
		return progList;
	}
	
	public void initializeProgress(User userToAdd)
	{
		try
		{
			PreparedStatement s = null;
			String query;
			query = "INSERT INTO progress VALUES (?,?,?) ";
			
			s=connection.prepareStatement(query);
			s.setInt(1, Integer.parseInt(userToAdd.getUserNo()));
			s.setInt(2, 0);
			s.setInt(3, 0);
			s.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void updateTopicProg(User loginUser, int topicUpdate)
	{
		try
		{
			PreparedStatement s = null;
			String query;
			query = "UPDATE progress SET topicProg = ? WHERE userNo = ?";
			
			s=connection.prepareStatement(query);
			s.setInt(1, topicUpdate);
			s.setInt(2, Integer.parseInt(loginUser.getUserNo()));
			s.executeUpdate();
			
			initializeList();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void updateChaProg(User loginUser, int chaUpdate)
	{
		try
		{
			PreparedStatement s = null;
			String query;
			query = "UPDATE progress SET chaProg = ? WHERE userNo = ?";
			
			s=connection.prepareStatement(query);
			s.setInt(1, chaUpdate);
			s.setInt(2, Integer.parseInt(loginUser.getUserNo()));
			s.executeUpdate();
			
			initializeList();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public Connection getConnection ()
	{
		return connection;
	}
	
	public void setConnection (Connection connection)
	{
		this.connection = connection;
	}
}
