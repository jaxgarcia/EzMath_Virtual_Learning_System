package techServ;

import java.sql.*;
import java.util.*;

import domain.User;

public class UserDA 
{
	private List <User> userList;
	private ResultSet rs;
	private User user;
	private Connection connection;
	private String loginUser="";
	
	public UserDA (Connection connection, String loginUser)
	{
		this.loginUser = loginUser;
		this.connection = connection;
		initializeList();
	}
	
	public UserDA(Connection connection)
	{
		this.connection = connection;
		initializeList();
	}
	
	public void initializeList()
	{
		try
		{
			userList = new ArrayList<User>();
			PreparedStatement s = null;
			String query;
			query = "SELECT * FROM user ORDER BY 1";
					
			s = connection.prepareStatement(query);
			rs = s.executeQuery();
					
			while (rs.next())
			{			
				user = new User();
				
				user.setUserNo(rs.getString(1));
				user.setUserName(rs.getString(2));
				
				userList.add(user);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public List<User> getList()
	{
		return userList;
	}
	
	public User getUserToValidate(User userToValidate)
	{
		User validUser = new User ();
		for (User testUser : userList)
		{
			if (testUser.getUserName().equals(userToValidate.getUserName()))
			{
				validUser = testUser;
				break;
			}
		}
		return validUser;
	}
	
	public void addUser(User userToAdd)
	{
		try
		{
			PreparedStatement s = null;
			String query;
			query = "INSERT INTO user (name) VALUES (?) ";
			
			s=connection.prepareStatement(query);
			s.setString(1, userToAdd.getUserName());
			s.executeUpdate();
			
			initializeList();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public Connection getConnection ()
	{
		return connection;
	}
	
	public void setConnection (Connection connection)
	{
		this.connection = connection;
	}
}